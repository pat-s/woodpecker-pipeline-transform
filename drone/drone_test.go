// Copyright 2022 Lauris BH. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package drone_test

import (
	"os"
	"testing"

	transform "codeberg.org/lafriks/woodpecker-pipeline-transform"
	"codeberg.org/lafriks/woodpecker-pipeline-transform/drone"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func getPipelineByName(pipelines []*transform.Pipeline, name string) *transform.Pipeline {
	for _, pipeline := range pipelines {
		if pipeline.Name == name {
			return pipeline
		}
	}
	return nil
}

func TestTransformSimple(t *testing.T) {
	buf, err := os.ReadFile("testdata/.drone.yml")
	require.NoError(t, err)

	d := drone.New()
	pipelines, err := d.Transform([]*transform.Source{
		{
			Name:    "simple",
			Content: buf,
		},
	})

	require.NoError(t, err)
	require.Len(t, pipelines, 3)

	pipeline := getPipelineByName(pipelines, "build")
	require.NotNil(t, pipeline, "build pipeline not found")

	require.Len(t, pipeline.Services, 1)
	assert.Equal(t, "database", pipeline.Services[0].Name)
	assert.Equal(t, "postgres:latest", pipeline.Services[0].Image)
	assert.True(t, pipeline.Services[0].Pull)
	assert.Equal(t, []string{"docker-entrypoint.sh postgres"}, pipeline.Services[0].Commands)
	require.NotNil(t, pipeline.Workspace)
	assert.Equal(t, "/go/src", pipeline.Workspace.Base)
	assert.Equal(t, "/go/src", pipeline.Workspace.Path)

	assert.Equal(t, "linux/amd64", pipeline.Platform)
	assert.Equal(t, map[string]string{"location": "europe"}, pipeline.Labels)

	require.NotNil(t, pipeline.Clone)
	require.NotNil(t, pipeline.Clone.Git)
	assert.Equal(t, "woodpeckerci/plugin-git", pipeline.Clone.Git.Image)
	assert.ElementsMatch(t, transform.Settings{transform.Setting{Name: "depth", Value: 50}}, pipeline.Clone.Git.Settings)

	require.Len(t, pipeline.Steps, 3)
	assert.Equal(t, "test", pipeline.Steps[0].Name)
	assert.Equal(t, "golang:1.18", pipeline.Steps[0].Image)
	assert.ElementsMatch(t, []string{"CGO=0"}, pipeline.Steps[0].Environment)
	assert.True(t, pipeline.Steps[0].Pull)
	assert.False(t, pipeline.Steps[0].Detach)
	assert.False(t, pipeline.Steps[0].Privileged)
	assert.Len(t, pipeline.Steps[0].Commands, 1)
	require.NotNil(t, pipeline.Steps[0].When)
	assert.ElementsMatch(t, []string{"push", "pull_request"}, pipeline.Steps[0].When.Event)
	assert.Empty(t, pipeline.Steps[0].When.Branch.Conditions)
	assert.Empty(t, pipeline.Steps[0].When.Branch.Include)
	assert.ElementsMatch(t, []string{"main", "release/*"}, pipeline.Steps[0].When.Branch.Exclude)

	assert.Equal(t, "build", pipeline.Steps[1].Name)
	assert.Equal(t, "golang:1.18", pipeline.Steps[1].Image)
	assert.False(t, pipeline.Steps[1].Pull)
	assert.False(t, pipeline.Steps[1].Detach)
	assert.False(t, pipeline.Steps[1].Privileged)
	assert.ElementsMatch(t, []string{"CGO=0", "GOOS=linux", "GOARCH=amd64"}, pipeline.Steps[1].Environment)
	assert.Len(t, pipeline.Steps[1].Commands, 1)
	assert.Nil(t, pipeline.Steps[1].When)

	assert.Equal(t, "docker", pipeline.Steps[2].Name)
	assert.Equal(t, "plugin/docker", pipeline.Steps[2].Image)
	assert.False(t, pipeline.Steps[2].Pull)
	assert.ElementsMatch(t, transform.Settings{
		{Name: "repo", Value: "org/simple"},
		{Name: "username", Value: map[string]interface{}{
			"from_secret": "docker_username",
		}},
		{Name: "password", Value: map[string]interface{}{
			"from_secret": "docker_password",
		}},
	}, pipeline.Steps[2].Settings)
	assert.ElementsMatch(t, []string{"/var/lib/cache:/var/lib/docker"}, pipeline.Steps[2].Volumes)

	assert.Len(t, pipeline.DependsOn, 0)

	pipeline = getPipelineByName(pipelines, "deploy")
	require.NotNil(t, pipeline, "deploy pipeline not found")

	assert.True(t, pipeline.SkipClone)
	assert.ElementsMatch(t, []string{"success"}, pipeline.RunsOn)

	require.Len(t, pipeline.Steps, 1)
	assert.Equal(t, "deploy", pipeline.Steps[0].Name)
	assert.Equal(t, "alpine:latest", pipeline.Steps[0].Image)
	assert.False(t, pipeline.Steps[0].Pull)
	assert.True(t, pipeline.Steps[0].Detach)
	assert.True(t, pipeline.Steps[0].Privileged)
	assert.Len(t, pipeline.Steps[0].Commands, 1)
	assert.ElementsMatch(t, transform.Secrets{{Target: "PASSWORD", Source: "password"}}, pipeline.Steps[0].Secrets)
	require.NotNil(t, pipeline.Steps[0].When)
	assert.ElementsMatch(t, []string{"success", "failure"}, pipeline.Steps[0].When.Status)
	assert.ElementsMatch(t, []string{"tag"}, pipeline.Steps[0].When.Event)
	assert.Equal(t, "{v*,RELEASE-*}", pipeline.Steps[0].When.Tag)
	assert.Equal(t, "stage.woodpecker.company.com", pipeline.Steps[0].When.Instance)
	assert.Equal(t, "staging", pipeline.Steps[0].When.Environment)

	assert.ElementsMatch(t, []string{"build"}, pipeline.DependsOn)

	pipeline = getPipelineByName(pipelines, "update-nlb")
	require.NotNil(t, pipeline, "update-nlb pipeline not found")

	assert.True(t, pipeline.SkipClone)

	require.Len(t, pipeline.Steps, 1)
	assert.Equal(t, "update", pipeline.Steps[0].Name)
	assert.Equal(t, "bash", pipeline.Steps[0].Image)
	assert.Len(t, pipeline.Steps[0].Commands, 1)

	assert.ElementsMatch(t, []string{"deploy"}, pipeline.DependsOn)
}

func TestPostProcess(t *testing.T) {
	d := drone.New()
	buf, err := d.PostProcess([]byte(`pipeline:
  step1:
	  image: bash
		commands:
		- echo "Test $${DRONE_COMMIT_SHA:0:7}"
`))
	require.NoError(t, err)
	assert.Equal(t, `pipeline:
  step1:
	  image: bash
		commands:
		- echo "Test $${CI_COMMIT_SHA:0:7}"
`, string(buf))
}
